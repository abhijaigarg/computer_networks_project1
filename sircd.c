#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include "debug.h"
#include "rtlib.h"
//#include "rtgrading.h"
#include "sircd.h"
#include "irc_proto.h"

// for select

u_long curr_nodeID;
rt_config_file_t   curr_node_config_file;  /* The config_file  for this node */
rt_config_entry_t *curr_node_config_entry; /* The config_entry for this node */



void init_node(char *nodeID, char *config_file);
void irc_server();
void write_nonblocking(char *line, client_node *clients, int clientID);
void clear_buffer(client_node *clients, int clientID);

client_node init_client_node();



void init_channels();

void
usage() {
    fprintf(stderr, "sircd [-h] [-D debug_lvl] <nodeID> <config file>\n");
    exit(-1);
}

void err_sys(const char* x){
    perror(x);
    exit(-1);
}

void err_quit(const char* x){
    err_sys(x);
}



int main( int argc, char *argv[] )
{

    extern char *optarg;
    extern int optind;
    int ch;
    
    while ((ch = getopt(argc, argv, "hD:")) != -1)
        switch (ch) {
            case 'D':
	            if (set_debug(optarg)) {
	               	exit(0);
	            }
	            break;
            case 'h':
            default: /* FALLTHROUGH */
                usage();
        }
    argc -= optind;
    argv += optind;

    if (argc < 2) {
	   usage();
    }
    
    init_node(argv[0], argv[1]);
    
    printf( "I am node %d and I listen on port %d for new users\n", curr_nodeID, curr_node_config_entry->irc_port );

    /* Start your engines here! */

    irc_server(); /* call to irc_server */
    
    return 0;
}

void irc_server(){

    int i;                          /* counter variable */
    int maxi;                       /* max number of clients */
    int maxfd;                      /* max file descriptors */
    int listenfd;                   /* listening file descriptor */
    int connfd;                     /* connection file descriptor */
    int sockfd;                     /* socket file descriptor */
    int nready;                     /* number of ready */
    client_node clients[MAX_CLIENTS]; /* array of structs to store clients */
    channel channels[MAX_CLIENTS]; /* since our implementation only has max 
                                    * one channel per user
                                    */

    ssize_t n;
    fd_set rset, wset, allset;
    char buf[MAX_BUF_SIZE+1];
    socklen_t clilen;
    struct sockaddr_in cliaddr, servaddr;

    int j, temp_sockfd, tempn;
    char tempbuf[MAX_MSG_LEN+1];

    int val;  /* variable to declare non blocking */

    char *token;
    char delim[3] = "\r\n";


    /* create socket, binder and listener */

    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(curr_node_config_entry->irc_port);

    //bind
    bind(listenfd, (struct sockaddr*) &servaddr, sizeof(servaddr));

    listen(listenfd, MAX_CLIENTS);

    
    maxfd = listenfd;                   /* initialise */
    maxi = -1;                          /* index into client array */

    for(i = 0; i < MAX_CLIENTS; i++){
        clients[i] = init_client_node();
    }
    init_channels(channels);

    FD_ZERO(&allset);
    FD_SET(listenfd, &allset);

    for( ; ; ){
        // printf("Entered here\n");
        rset = allset;                            /* structure assignment */
        nready = select(maxfd + 1, &rset, NULL, NULL, NULL);

        if (FD_ISSET(listenfd, &rset)) { /* new client connection */
            clilen = sizeof(cliaddr);
            connfd = accept(listenfd, (struct sockaddr*) &cliaddr, &clilen);


            for(i = 0; i < MAX_CLIENTS; i++) {
                if (clients[i].sock < 0){
                                       
                    val = fcntl(connfd, F_GETFL, 0);         /* set to non blocking */
                    
                    fcntl(connfd, F_SETFL, val | O_NONBLOCK);

                    clients[i].sock = connfd;  /* save descriptor */    

                    clients[i].registered = -1; /* update registered variable because client is now registered */
                    clients[i].friptr = clients[i].froptr = clients[i].fr;
                    strcpy(clients[i].nick,"*");
                    break;
                }
            }

            if (i == MAX_CLIENTS)
                err_sys("too many clients");

            FD_SET(connfd, &allset); /* add new descriptor to set */

            if(connfd > maxfd)
                maxfd = connfd; /* for select */

            if ( i > maxi)
                maxi = i;   /* max index in client array */

            if (--nready <= 0)
                continue;   /* no readable descriptors */

            
        }
        // printf("checkpoint 1\n");

        for (i = 0; i <= maxi; i++){ /* check all the clients for data */
            // printf("Checking all clients for data\n" );
            memset(buf, '\0', sizeof(buf));
            if ( (sockfd = clients[i].sock) < 0) /* this means no client fd registered */
                continue;

            if (FD_ISSET(sockfd, &rset)){ /* if fd exists */
                // printf("checkpoint 2\n");
                if ( (n = read(sockfd, buf, MAX_BUF_SIZE+1)) == 0){
                    /* connection closed by client */
                    // printf("checkpoint 3\n");
                    handle_line("QUIT", i, &clients, channels, &allset);
                }

                else if (n < 0){
                    // printf("checkpoint 4\n");
                    if (errno != EWOULDBLOCK && errno != EINTR)
                        err_sys(strerror(errno));

                }

                else
                {
                    
                    // printf("checkpoint 4\n");
                    /* non blocking write */

                    if(n > MAX_MSG_LEN){
                        n = MAX_MSG_LEN;  //truncate if more than MAX_MSG_LEN
                        buf[n-1] = '\0';
                    }

                    token = strtok(buf, delim);    /* strip trailing \n */                    
                    
                    /* iterate over line_to_handle */
                    while(token != NULL){
                        handle_line(token, i, &clients, channels, &allset);
                        token = strtok(NULL, delim); 
                    }

                }

                clear_buffer(clients, i); /* check buffer and clear if anything inside */

                if(--nready <= 0)
                    break; /* no more readable descriptors */


            }

        }       

    }

}

/*
 *void irc_server()
 *
 * Function call to the engines 
 */

/*
 * void init_node( int argc, char *argv[] )
 *
 * Takes care of initializing a node for an IRC server
 * from the given command line arguments
 */
void
init_node(char *nodeID, char *config_file) 
{
    int i;

    curr_nodeID = atol(nodeID);
    rt_parse_config_file("sircd", &curr_node_config_file, config_file );

    /* Get config file for this node */
    for( i = 0; i < curr_node_config_file.size; ++i )
        if( curr_node_config_file.entries[i].nodeID == curr_nodeID )
             curr_node_config_entry = &curr_node_config_file.entries[i];

    /* Check to see if nodeID is valid */
    if( !curr_node_config_entry )
    {
        printf( "Invalid NodeID\n" );
        exit(1);
    }
}

/*
 *
 *
 */
client_node init_client_node(){
    
    client_node node;

    node.sock                   = -1;    
    bzero(&node.cliaddr,sizeof(node.cliaddr)); /* make all bytes = 0 */
    node.registered             = -1; /* nick and user not registered 
                                       * if user is registered but nick
                                       * is not, value is still -1
                                       */

    node.user_registered        = -1;
    node.channelID              = -1; /* no channel */
    memset(node.hostname,   '\0', sizeof(node.hostname));
    memset(node.servername, '\0', sizeof(node.servername));
    memset(node.realname,   '\0', sizeof(node.realname));
    memset(node.user, '\0', sizeof(node.user));
    memset(node.nick, '\0', sizeof(node.nick));
    memset(node.fr,   '\0', sizeof(node.fr));

    node.friptr = node.froptr = node.fr;

    return node;
}

/*
 * init_channels() : initialise channels array 
 */
void init_channels(channel *channels){
    int i;
    for (i = 0; i < MAX_CLIENTS; i++){
            channels[i].id = i;
            channels[i].clients = 0;
    }
}

/*
 * write_nonblocking implementation
 */

void write_nonblocking(char *line, client_node *clients, int clientID){ /* non-blocking writing */
    
    int buf_size = strlen(line);
    int nwritten;

    /* write message and append with \n and \0 to terminate string */

    strncpy(clients[clientID].friptr, line, buf_size);


    (clients[clientID].friptr)[buf_size] = '\n'; /* append new line and \0 null terminator */
    (clients[clientID].friptr)[buf_size + 1] = '\0';
    

    if ( nwritten = write(clients[clientID].sock, clients[clientID].froptr, buf_size + 2) < 0) {
        if (errno != EWOULDBLOCK)
            err_sys(strerror(errno));

        else if(errno == EWOULDBLOCK)
          printf("EWOULDBLOCK\n");
    }
    else{

        clients[clientID].friptr += buf_size + 2;
        clients[clientID].froptr += nwritten; 

        if (clients[clientID].froptr == clients[clientID].friptr) {
            clients[clientID].froptr = clients[clientID].friptr = clients[clientID].fr;
        }
    }

}


void clear_buffer(client_node *clients, int clientID){

    int n, nwritten;
    int i;
    for (i = 0; i < MAX_CLIENTS; i++){ /* check all buffers */
        n = 0;
        if(clients[i].sock > 0){
            n = clients[i].friptr - clients[i].froptr;

            if(clients[i].froptr != clients[i].friptr){

                if((nwritten = write(clients[i].sock, clients[i].froptr, n)) < 0){
                  
                    if(errno != EWOULDBLOCK){
                        err_sys("write error to socket");
                    }
                }
                else{

                    clients[i].froptr += nwritten;

                    if(clients[i].froptr == clients[i].friptr){

                        clients[i].friptr = clients[i].froptr = clients[i].fr; /* revert to beginning of buffer*/
                        memset(clients[i].fr, '\0',sizeof(clients[i].fr));
                    }
                }
            } 
        } 
    }
    

    
}