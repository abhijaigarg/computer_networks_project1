#include "irc_proto.h"
#include "debug.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_COMMAND 16

/* You'll want to define the CMD_ARGS to match up with how you
 * keep track of clients.  Probably add a few args...
 * The command handler functions will look like
 * void cmd_nick(CMD_ARGS)
 * e.g., void cmd_nick(your_client_thingy *c, char *prefix, ...)
 * or however you set it up.
 */

#define CMD_ARGS char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset
typedef void (*cmd_handler_t)(CMD_ARGS);
#define COMMAND(cmd_name) void cmd_name(CMD_ARGS)

struct dispatch {
    char cmd[MAX_COMMAND];
    int needreg; /* Must the user be registered to issue this cmd? */
    int minparams; /* send NEEDMOREPARAMS if < this many params */
    cmd_handler_t handler;
};


#define NELMS(array) (sizeof(array) / sizeof(array[0]))

/* Define the command handlers here.  This is just a quick macro
 * to make it easy to set things up */
COMMAND(cmd_nick);
COMMAND(cmd_user);
COMMAND(cmd_quit);
COMMAND(cmd_join);
COMMAND(cmd_part);
COMMAND(cmd_list);
COMMAND(cmd_privmsg);
COMMAND(cmd_who);
/* Dispatch table.  "reg" means "user must be registered in order
 * to call this function".  "#param" is the # of parameters that
 * the command requires.  It may take more optional parameters.
 */
struct dispatch cmds[] = {
    /* cmd,    reg  #parm  function */
    { "NICK",    0, 1, cmd_nick },
    { "USER",    0, 4, cmd_user },
    { "QUIT",    0, 0, cmd_quit },
    { "JOIN",    1, 1, cmd_join },
    { "PART",    1, 1, cmd_part },
    { "LIST",    0, 0, cmd_list },
    { "PRIVMSG", 1, 1, cmd_privmsg },
    { "WHO",     0, 1, cmd_who }
};

/* Handle a command line.  NOTE:  You will probably want to
 * modify the way this function is called to pass in a client
 * pointer or a table pointer or something of that nature
 * so you know who to dispatch on...
 * Mostly, this is here to do the parsing and dispatching
 * for you
 *
 * This function takes a single line of text.  You MUST have
 * ensured that it's a complete line (i.e., don't just pass
 * it the result of calling read()).  
 * Strip the trailing newline off before calling this function.
 */

void throw_error(char *error, err_t errorno, int clientID, client_node *clients);
void print_message(char *msg, int clientID, client_node *clients);
void send_message(char *msg, int sender, int receiver, client_node *clients); 
void channel_broadcast(char * msg, client_node *clients, int channelID, int num_clients);

void
handle_line(char *line, int clientID, client_node * clients, channel * channels, fd_set * allset)
{
    char *prefix = NULL, *command, *pstart, *params[MAX_MSG_TOKENS];
    int n_params = 0;
    char *trailing = NULL;

 

    DPRINTF(DEBUG_INPUT, "Handling line: %s\n", line);
    command = line;
    if (*line == ':') {
	prefix = ++line;
	command = strchr(prefix, ' ');
    }
    if (!command || *command == '\0') {
		/* Send an unknown command error! */
		throw_error("ERR_UNKNOWNCOMMAND 1", ERR_UNKNOWNCOMMAND, clientID, clients);
		return;
    }
    
    while (*command == ' ') {
		*command++ = 0;
    }
    if (*command == '\0') {
		/* Send an unknown command error! */
		throw_error("ERR_UNKNOWNCOMMAND 2", ERR_UNKNOWNCOMMAND, clientID, clients);
		return;
    }
    pstart = strchr(command, ' ');
    if (pstart) {
	while (*pstart == ' ') {
	    *pstart++ = '\0';
	}
	if (*pstart == ':') {
	    trailing = pstart;
	} else {
	    trailing = strstr(pstart, " :");
	}
	if (trailing) {
	    while (*trailing == ' ')
		*trailing++ = 0;
	    if (*trailing == ':')
		*trailing++ = 0;
	}
	
	do {
	    if (*pstart != '\0') {
		params[n_params++] = pstart;
	    } else {
		break;
	    }
	    pstart = strchr(pstart, ' ');
	    if (pstart) {
		while (*pstart == ' ') {
		    *pstart++ = '\0';
		}
	    }
	} while (pstart != NULL && n_params < MAX_MSG_TOKENS);
    }

    if (trailing && n_params < MAX_MSG_TOKENS) {
	params[n_params++] = trailing;
    }
    
    DPRINTF(DEBUG_INPUT, "Prefix:  %s\nCommand: %s\nParams (%d):\n",
	    prefix ? prefix : "<none>", command, n_params);
    int i;
    for (i = 0; i < n_params; i++) {
		DPRINTF(DEBUG_INPUT, "   %s\n", params[i]);
    }
    DPRINTF(DEBUG_INPUT, "\n");

    for (i = 0; i < NELMS(cmds); i++) {
		if (!strcasecmp(cmds[i].cmd, command)) {
		    if (cmds[i].needreg && clients[clientID].registered != 1 ) { /* checking if client is registered */
			        /* ERROR THAT CLIENT IS NOT REGISTERED */

					throw_error("ERR_NOTREGISTERED", ERR_NOTREGISTERED, clientID, clients);
			    	return;

				/* ERROR - the client is not registered and they need
				 * to be in order to use this command! */

		    } else if (n_params < cmds[i].minparams) {
				/* ERROR - the client didn't specify enough parameters
				 * for this command! */
					throw_error("ERR_NEEDMOREPARAMS", ERR_NEEDMOREPARAMS, clientID, clients);
			    	return;
		    } else {
				/* Here's the call to the cmd_foo handler... modify
				 * to send it the right params per your program
				 * structure. */
				(*cmds[i].handler)(prefix, params, n_params, clientID, clients, channels, allset);
		    }
		    break;
		}
    }
    if (i == NELMS(cmds)) {
		/* ERROR - unknown command! */
		throw_error("ERR_UNKNOWNCOMMAND 3", ERR_UNKNOWNCOMMAND, clientID, clients);
    	return;
    }
}



/* Command handlers */

/* 
 * cmd_nick()
 * set nick name as long as there is no conflict
 */
void cmd_nick(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset)
{
    /*  
	 * params[0] = nick name
     */

	int i;
	char msg[MAX_MESSAGE_LEN];
	memset(msg, '\0', sizeof(msg));

    if( clients[clientID].user_registered < 1){
    	throw_error("ERR_NOTREGISTERED", ERR_NOTREGISTERED, clientID, clients);
    }
    else{
    	for(i = 0; i < MAX_CLIENTS; i++)/* check if there is a nick conflict */
    	{
    		if (i != clientID){ /* client should not check against itself */
	    		if(clients[i].registered > 0 && strcasecmp(clients[i].nick, params[0]) == 0){
	    			throw_error("ERR_ERRONEOUSNICKNAME", ERR_ERRONEOUSNICKNAME, clientID, clients);
	    			break;
	    		}
	    	}
    	} 
    	if(i == MAX_CLIENTS){ /* message the client telling it that nick name is registered. */
	    	strcpy(clients[clientID].nick, params[0]);
	    	sprintf(msg, ":%s NICK_REGISTERED", clients[clientID].nick);
    		print_message(msg, clientID, clients);
    		clients[clientID].registered = 1;
    	}
    }

    return;
}

/*
 * cmd_user()
 * set the <username> <hostname> <servername> <realname> for user
 */

void cmd_user(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset)
{
	/*
	 * params[0] - username
	 * params[1] - hostname
	 * params[2] - servername
	 * params[3] - realname
	 */
	sprintf(clients[clientID].hostname,"%s", params[1]);
	sprintf(clients[clientID].servername,"%s", params[2]);
	sprintf(clients[clientID].user,"%s", params[0]);
	sprintf(clients[clientID].realname,"%s", params[3]);

	clients[clientID].user_registered = 1;
	
	print_message("USER_RECVD_ACCEPTED", clientID, clients);
	return;
}	

/*
 * cmd_quit()
 * quit the client. close descriptor and make changes in fd_set allset
 */

void cmd_quit(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset)
{
	char msg[MAX_MESSAGE_LEN];
	memset(msg, '\0', sizeof(msg));

	if ( n_params == 0){
		sprintf(msg, ":%s QUIT :client quit session", clients[clientID].nick);
	}
	else if(n_params == 1){
		sprintf(msg, ":%s QUIT :%s", clients[clientID].nick, params[0]);
	}

	channel_broadcast(msg, clients, clients[clientID].channelID, channels[clients[clientID].channelID].clients);

	close(clients[clientID].sock);
	FD_CLR(clients[clientID].sock, allset);
	clients[clientID].registered = -1;
	clients[clientID].user_registered = -1;
	clients[clientID].sock = -1;
	clients[clientID].channelID = -1;

	return;
}

/*
 * cmd_join()
 * join a new channel. accept only one channel per user
 */
void cmd_join(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset){
	/* params[0]: name of channel */
	int i, j, channelID = -1;
	char msg[MAX_MESSAGE_LEN], part_msg[MAX_MESSAGE_LEN];

	memset(msg, '\0', sizeof(msg));
	memset(part_msg, '\0', sizeof(msg));

	strcpy(msg, "");

	
	if (clients[clientID].registered > 0) /* if client is registered */
	{
		/* if name exceeds max channel name size */
		if(strlen(params[0]) > MAX_CHANNAME)
		{
			throw_error("ERR_CHANNNAME_TOO_LONG", ERR_INVALID, clientID, clients);
			return;
		}

		
		if( clients[clientID].channelID > -1 ) /* there is a channel assigned */
		{
			channelID = clients[clientID].channelID;
		}

		for (i = 0; i < MAX_CLIENTS; i++) /* check if there is existing channel
											* with same name
											*/
		{
			if((strcmp(params[0], channels[i].channel) == 0) && channels[i].clients > 0)
			{
				if(channelID > -1){ /* if you have channel already
										  * part from existing channel first
										  */	
					
					sprintf(part_msg, ":%s PARTED %s", clients[clientID].nick, channels[channelID].channel);
					channel_broadcast(part_msg, clients, clients[clientID].channelID, channels[channelID].clients);
					channels[channelID].clients--;
					clients[clientID].channelID = -1; 
					
				}

				channels[i].clients++;
				clients[clientID].channelID = channels[i].id;
				sprintf(msg, ":%s JOINED CHANNEL %s", clients[clientID].nick,channels[i].channel);
				channel_broadcast(msg, clients, clients[clientID].channelID, channels[channelID].clients);
				
				break;	

			}
		}

		if(i == MAX_CLIENTS) /* channel does not exist */
			/* create new channel */
		{
			if(channelID > -1){ /* if you have channel already
									  * part from existing channel first
									  */	
				channels[channelID].clients--;
				clients[clientID].channelID = -1; 

				sprintf(part_msg, ":%s PARTED %s", clients[clientID].nick, channels[channelID].channel);
				channel_broadcast(part_msg, clients, clients[clientID].channelID, channels[channelID].clients);		
			}

			for (j = 0; j < MAX_CLIENTS; j++) /* look for open channel */
			{	
				if(channels[j].clients == 0)
				{
					channels[j].clients++;
					clients[clientID].channelID = channels[j].id;
					sprintf(channels[j].channel, "%s", params[0]);
					break;
				}
			}
			if (j == MAX_CLIENTS)
			{					
				throw_error("ERR_CHAN_CANT_CREATE", ERR_INVALID, clientID, clients);
				return;
			}
			sprintf(msg, ":%s JOINED CHANNEL %s", clients[clientID].nick, channels[clients[clientID].channelID].channel);
			
			channel_broadcast(msg, clients, clients[clientID].channelID, channels[clients[clientID].channelID].clients);
		}
	}
	else{
		throw_error("ERR_NOTREGISTERED", ERR_NOTREGISTERED, clientID, clients);
	}

	return;
	
}


/*
 * cmd_part()
 * part from existing channel if any and let everyone know.
 */

void cmd_part(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset){
	/* params[0] */

	char msg[MAX_ERRORCODE_LEN];
	memset(msg, '\0', sizeof(msg));
	if(strcmp(channels[clients[clientID].channelID].channel,params[0]) == 0) 
	{															/* check if client gives valid channel */
		channels[clients[clientID].channelID].clients--;
		sprintf(msg, ":%s PARTED %s", clients[clientID].nick, channels[clients[clientID].channelID].channel);
		channel_broadcast(msg, clients, clients[clientID].channelID, channels[clients[clientID].channelID].clients);
		clients[clientID].channelID = -1;
	}
	else{
		throw_error("ERR_NOTONCHANNEL", ERR_NOTONCHANNEL, clientID, clients);
	}
	return;
}

/*
 * cmd_list()
 * look for all valid channel and display number of users on it
 */

void cmd_list(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset){
	char msg[MAX_MSG_LEN], tempmsg[MAX_MSG_LEN];
	memset(msg, '\0', sizeof(msg));
	int i;
	int count = 0;
	strcpy(msg, "");
	for(i = 0 ; i < MAX_CLIENTS; i++){ /* go though all the channels */
		if(channels[i].clients > 0){
			memset(tempmsg, '\0', sizeof(tempmsg));
			sprintf(tempmsg, ":%d: <%s> %d\n", channels[i].id, channels[i].channel, channels[i].clients);

			strcat(msg, tempmsg);
			
			count++;
		}	
	}

	if( count == 0)
		strcat(msg, "Error 1: <no channels yet>\n");

	print_message(msg, clientID, clients);
	return;
}

/*
 * cmd_privmsg()
 * part from existing channel if any and let everyone know.
 */

void cmd_privmsg(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset){
	int i;
	int channelID = -1;
	int channelCount = -1;

	int destclientID = -1;

	char msg[MAX_MSG_LEN];

	for(i = 0; i < MAX_CLIENTS; i++){ /* check channels first */
		if(channels[i].clients > 0 && (strcmp(channels[i].channel, params[0]) == 0)){
			channelID = channels[i].id;
			channelCount = channels[i].clients;
			break;
		}
	}
	
	if (channelID >= 0){ /* if user wants to send message to channel */
		memset(msg, '\0', sizeof(msg));
		sprintf(msg, ":%s PRIVMSG %s :%s", clients[clientID].nick, params[0], params[1]);
		channel_broadcast(msg, clients, channelID, channels[channelID].clients);
	}
	else if (channelID < 0){ /*it is not a channel */
		for (i = 0; i < MAX_CLIENTS; i++){ /* find if nick matches params[0] */
			if(clients[i].registered > 0 && (strcasecmp(clients[i].nick, params[0]) == 0)){
				destclientID = i;
				send_message(params[1], clientID, i, clients); 
				break;
			}
		}
		if (destclientID < 0){ /* if no such nick */
			throw_error("ERR_NOSUCHNICK", ERR_NOSUCHNICK, clientID, clients);
		}
	}
	return;
	
}

/*
 * cmd_who()
 * returns all users in channel(s)
 */

void cmd_who(char *prefix, char **params, int n_params, int clientID, client_node *clients, channel *channels, fd_set *allset){
	int i;
	int channelID = -1;
	int channelCount = -1;
	char msg[MAX_MSG_LEN];
	memset(msg, '\0', sizeof(msg));
	/* assuming there is no whitespace after ',' separator */

	char *token;

	token = strtok(params[0], ",");
	strcpy(msg, "");
	while(token != NULL){ /* implement for each channel listed */
		for (i = 0; i < MAX_CLIENTS; i++){ /* go through all channels */
			if(strcmp(channels[i].channel, token) == 0 && channels[i].clients > 0){ /* look for exact match */
				channelID = channels[i].id;
				channelCount = channels[i].clients;
				break;
			}
		}

		if(i == MAX_CLIENTS){
			strcat(msg, "<");
			strcat(msg, token);
			strcat(msg, ">: ERR_NOSUCHCHANNEL\n");
		}

		else if(channelID >= 0 && channels[channelID].clients > 0){ /* succeeded in finding matching channel */
			strcat(msg, "<");
			strcat(msg, channels[channelID].channel);
			strcat(msg, ">: ");
			for (i = 0; i < MAX_CLIENTS; i++){ /* look for matching clients */
				if ( clients[i].channelID == channelID){
					strcat(msg, " ");
					strcat(msg, clients[i].nick);
				}
			}
			strcat(msg, "\n");
			
		}

		token = strtok(NULL, ",");
	}
	print_message(msg, clientID, clients);

	return;
}


void throw_error(char *error, err_t errorno, int clientID, client_node *clients){ /* throw errors */
	char error_msg[MAX_ERRORCODE_LEN];
	memset(error_msg, '\0', sizeof(error_msg));

	sprintf(error_msg, "Error %d: %s", errorno, error);
	// write(clients[clientID].sock, error_msg, sizeof(error_msg));
	write_nonblocking(error_msg, clients, clientID);	
}

void print_message(char *msg, int clientID, client_node *clients){ /* give messages to client(s) */
	
	char msg_to_print[MAX_MESSAGE_LEN];
	memset(msg_to_print, '\0', sizeof(msg_to_print));

	sprintf(msg_to_print, "%s", msg);
	
	// write(clients[clientID].sock, msg_to_print, sizeof(msg_to_print));

	write_nonblocking(msg_to_print, clients, clientID);
}

void send_message(char *msg, int sender, int receiver, client_node *clients){ /* for PRIVMSG */
	char final_msg[MAX_MESSAGE_LEN];
	memset(final_msg, '\0', sizeof(final_msg));
	sprintf(final_msg,":%s PRIVMSG %s :%s", clients[sender].nick, clients[receiver].nick, msg);
	print_message(final_msg, receiver, clients);
	
}


void channel_broadcast(char * msg, client_node *clients, int channelID, int num_clients){ /* broadcast to all clients on channel */
	int i;
	for ( i = 0 ; i < MAX_CLIENTS; i++){
		if(clients[i].registered > 0 && clients[i].channelID == channelID){ /* if client exists on channel */
			print_message(msg, i, clients);
			num_clients--;
		}		
		if(num_clients <= 0)
			break;
	}
}