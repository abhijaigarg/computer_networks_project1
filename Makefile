# This makefile does NOT create the sircd for you.
# You'll want to compile debug.o into sircd to get
# the debugging functions.
# We suggest adding three targets:  all, clean, test

CC = gcc
CFLAGS = -I.
DEPS = sircd.h irc_proto.h rtlib.h debug.h
OBJ = sircd.o irc_proto.o rtlib.o debug.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

sircd: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean: 
	$(RM) $(OBJ)
