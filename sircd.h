#ifndef _SIRCD_H_
#define _SIRCD_H_

#include <sys/types.h>
#include <netinet/in.h>

#define MAX_BUF_SIZE 1024
#define MAX_CLIENTS 512
#define MAX_MSG_TOKENS 10
#define MAX_MSG_LEN 512
#define MAX_USERNAME 32
#define MAX_HOSTNAME 512
#define MAX_SERVERNAME 512
#define MAX_REALNAME 512
#define MAX_CHANNAME 512

// temp for EINTR
#define EINTR 4
typedef struct {
    int sock;                       /* file descriptor */
    struct sockaddr_in cliaddr;     /* client address information */
    //unsigned inbuf_size;            /* size of input line */
    int registered;                 /* is the client registered. -1 for no, 1 for yes */
    int user_registered;            /* only checks is user is assigned all values */
    char hostname[MAX_HOSTNAME];    /* host name of the client */
    char servername[MAX_SERVERNAME];/* server name the client is connected to */
    char user[MAX_USERNAME];        /* user name */
    char nick[MAX_USERNAME];        /* nick name of the user */
    char realname[MAX_REALNAME];    /* ip address if reverse lookup of ip fails */
    // char inbuf[MAX_MSG_LEN+1];      
    int channelID;     /* id of channel client is currently on */

    char *friptr, *froptr;
    char fr[MAX_MSG_LEN+1];
    
} client_node;

typedef struct{
    int id; /* id of the channel */
    char channel[MAX_CHANNAME];
    int clients; /* number of clients on this channel */
} channel;

void str_echo(int sockfd);
void err_sys(const char* x);

#endif 

/* _SIRCD_H_ */
