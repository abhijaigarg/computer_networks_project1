#**IRC Server - Abhijai Garg**#


2. ##**COMPONENTS**##
	
	###Client###
	Each client_node object contains the following atttributes

		typedef struct {
		int sock;                       /* file descriptor */
		struct sockaddr_in cliaddr;     /* client address information */
		int registered;                 /* is the client registered. -1 for no, 1 for yes */
		int user_registered;            /* only checks is user is assigned all values */
		char hostname[MAX_HOSTNAME];    /* host name of the client */
		char servername[MAX_SERVERNAME];/* server name the client is connected to */
		char user[MAX_USERNAME];        /* user name */
		char nick[MAX_USERNAME];        /* nick name of the user */
		char realname[MAX_REALNAME];    /* ip address if reverse lookup of ip fails */
		int channelID;     				/* id of channel client is currently on */

		char *friptr, *froptr;
		char fr[MAX_MSG_LEN+1];

		} client_node;

	The MAX_CLIENTS are defined in the _sircd.h_ by the framework code and an array of type _client_node_ and size MAX_CLIENTS is declared to keep track of the clients. The _registered_ parameter in the client tells if a specific location in the client array is allocated or not when new client tries to connect.


	###Channel###
	Each channel object contains the following attributes
		
		typedef struct{
		    int id; /* id of the channel */
		    char channel[MAX_CHANNAME];
		    int clients; /* number of clients on this channel */
		} channel;

	As per modified IRC_PROTOCOLS, each client is allowed at most one channel subscription, so the maximum number of channel will be the same as the maximum number of clients ie MAX_CLIENTS. So, like the clients, channels are also stored in an array of type _channel_ and size MAX_CLIENTS.

	Note that every channel has an _id_ and at initialisation stage in _init_channels_, the id is set to be the same as the index number of the channel in the channel array. Even though this is redundant, it will help change the format of channel numbers if needed.

	At initialisation, the _clients_ are set to 0 and this attribute counts the number of clients connected to a specific channel.



2. ##**COMMANDS**##
	
	###CMD_USER###

	The existing implementation of the _handle_line_ function in _irc_proto.h_ implements parameter handling etc. The task performed by CMD_USER is as simple as copying the <username> <hostname> <servername> <realname> into the respective attributes for the client object in the client array. At this stage, an attribute _user_registered_ is set to 1. This is a flag variable read by CMD_NICK to make sure that an incoming client has already provided full information. The client has the ability to overwrite the USER details.

	###CMD_NICK###
	NICK first checks if _user_registered_ = 1. If so, it lets client assign nickname.

	The nickname is checked against all active users and in case there is a name match, the user is returned an error.
	Additionally, client can change nicknames, so client cannot check against its own self.

		for(i = 0; i < MAX_CLIENTS; i++)/* check if there is a nick conflict */
    	{
    		if (i != clientID){ /* client should not check against itself */
	    		if(clients[i].registered > 0 && strcasecmp(clients[i].nick, params[0]) == 0){
	    			throw_error("ERR_ERRONEOUSNICKNAME", ERR_ERRONEOUSNICKNAME, clientID, clients);
	    			break;
	    		}
	    	}
    	} 

    The checking loop performs a _strcasecmp()_ which does a check against nick names irrespective of their cases. The aim is to minimise confusion caused by same names with different cases.s

    ###CMD_JOIN###
    ** Is client already connected to a channel?**
    CMD_JOIN first checks whether calling client is already connected to a channel. The client object saves the _channelID_ which is set to the corresponding _id_ of the channel in case of a connection, and to -1 in case client is not assigned to any channel. 
    In case client is indeed connected to a channel, we need to first disconnect (PART) it from that channel, let everyone on the channel know and then move on to connect to the newly requested channel. 
    	if(channelID > -1){ /* if you have channel already
								  * part from existing channel first
								  */	
			
			sprintf(part_msg, ":%s PARTED %s", clients[clientID].nick, channels[channelID].channel);
			channel_broadcast(part_msg, clients, clients[clientID].channelID, channels[channelID].clients);
			channels[channelID].clients--;
			clients[clientID].channelID = -1; 
			
		}
	Once it is disconnected from previous channel (if applicable), the new channel as requested by the user is searched. 
	**Channel already exists**: A linear search is performed for the channel name and on a hard match (no _strcasecmp()_ ), the _clients_ variable reflecting the number of connected users is increased and the _channelID_ for the calling client is set to the _id_ of the channel index in the channel array.

	**Channel does not exist**: In this case, the function looks for the next available client index in the client array and assigns this new channel to this index. There is also an implementation for a case where there will be no more indices available to write, and in this case there will be an error returned. However, since we only allow one channel per client, we will never run into this situation.

	###CMD_PART###
	CMD_PART exits from existing channel and broadcasts a parting message to every client connected to the channel. Additionally it returns an error in case PART is attempted without the existence of a channel connection to the calling client.

	###CMD_LIST###
	CMD_LIST is required to return all the active channels with number of clients of each. Luckily, since we have all these values in the struct object for channel, this function call will simply involve a linear search through all the channels. In case an active channel (where there is >0 clients) is found, the _channel_ (name) and _clients_ are copied to a buffer.
	It also updates a variable _counter_ every time a channel is found. In case _counter=0_ at the end of the search, we know that there are no active channels.

	###CMD_PRIVMSG###
	CMD_PRIVMSG allows client to send message to a person or a channel.

	First the function searches through the list of channels to see if there is a matching channel in the channel array. If such a channel is found, we save the _id_ and _clients_. This message is then broadcasted to all the members of this Channel.
	In case, no matching channel name is found, the client nick names are looked up. Since we check for unique nicknames, we can terminate our search as soon as we find a match and simply update the file descriptor of this found client. 
	In case no matching client nick name is found, CMD_PRIVMSG returns an error

	###CMD_WHO###
	CMD_WHO handles multi argument, comma separated requests to display clients connected channels.	The output is the nick names of all the clients connected to active channels. 

	###CMD_QUIT###
	CMD_QUIT closes the file descriptor of the calling client and clears the relevant bit in the _allset_. Additionally it sends a message to all other clients connected to the same channel as calling client. In case the calling client isn't connected to any channel, no message is sent and the client just quits.

		memset(msg, '\0', sizeof(msg));

		if ( n_params == 0){
			sprintf(msg, ":%s QUIT :client quit session", clients[clientID].nick);
		}
		else if(n_params == 1){
			sprintf(msg, ":%s QUIT :%s", clients[clientID].nick, params[0]);
		}

		channel_broadcast(msg, clients, clients[clientID].channelID, channels[clients[clientID].channelID].clients);

		close(clients[clientID].sock);
		FD_CLR(clients[clientID].sock, allset);
		clients[clientID].registered = -1;
		clients[clientID].user_registered = -1;
		clients[clientID].sock = -1;
		clients[clientID].channelID = -1;

		return;

	The same function is called in case a client loses connection.



2. ##**USER DEFINED FUNCTIONS**##
	
	###channel_broadcast(char * msg, client_node *clients, int channelID, int num_clients)###
	channel_broadcast() performs a linear search on all clients and displays message to the client who has a matching channelID. A minor optimization has been performed in that the _num_client_ is decremented for every iteration that a mathcing _client_node_ is found. This ensures that once all _n_ users connected to the channel are accessed, the loop breaks and does not conduct an unnecessary search.

		for ( i = 0 ; i < MAX_CLIENTS; i++){
			if(clients[i].registered > 0 && clients[i].channelID == channelID){ /* if client exists on channel */
				print_message(msg, i, clients);
				num_clients--;
			}		
			if(num_clients <= 0)
				break;
		} 

	###write_nonblocking(char *line, client_node *clients, int clientID)###
	This function is implemented to support non-blocking write to the client.



	_client_node_ contains three variables :

	* _fr_ which is a char array that acts as a buffer for the client local to the server. This is where a message is written by default. After each write, '\n\0' are appended to the end of the message to separate it within the buffer.

	* _friptr_ keeps track of the number of bits that have been utilised in the _fr_ buffer.

	*_froptr_ keeps track of how many bits have been written to the client.

	_friptr_ & _froptr_ are pointers that are initialized to point to the starting of the array _fr_ and are updated as and when more information comes in. Also, when _friptr_ = _froptr_, the values are set back to _fr_.

	To enable non-blocking write, the _sock_ needs to be set to non-blocking. This happens when the client first connects to the server. 
		val = fcntl(connfd, F_GETFL, 0);         /* set to non blocking */
        fcntl(connfd, F_SETFL, val | O_NONBLOCK);

    We also handle for errors, but specify that if errno == EWOULDBLOCK, the program should just continue.

	###clear_buffer(client_node *clients, int clientID)###
	_clear_buffer()_ is called after every iteration over the file descriptors. It checks all the buffers of all the clients and tries to write the remaining number of bits to the client. If it is not able to succeed and if EWOULDBLOCK, it just skips and comes back to it later. 



